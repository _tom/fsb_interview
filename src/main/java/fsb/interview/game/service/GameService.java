package fsb.interview.game.service;

import fsb.interview.game.dto.GameDto;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class GameService {

  private final ConcurrentHashMap<String, GameDto> gameCache = new ConcurrentHashMap<>();

  public GameDto getGame(String name) {
    return this.gameCache.get(name);
  }

  public GameDto createGame(GameDto gameDto) {
    if (gameCache.containsKey(gameDto.getName())) {
      return null;
    } else {
      gameDto.setDateOfCreation(LocalDate.now());
      gameCache.put(gameDto.getName(), gameDto);
      return gameDto;
    }
  }

  public GameDto updateGame(GameDto gameDto) {
    if (gameCache.containsKey(gameDto.getName())) {
      gameCache.put(gameDto.getName(), gameDto);
      return gameDto;
    } else {
      return null;
    }
  }

  public boolean deleteGame(String name) {
    if (gameCache.containsKey(name)) {
      gameCache.remove(name);
      return true;
    } else {
      return false;
    }
  }
}
