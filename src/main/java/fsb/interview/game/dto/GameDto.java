package fsb.interview.game.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@AllArgsConstructor
@Data
public class GameDto {
  private String name;
  private LocalDate dateOfCreation;
  private boolean active;
}
