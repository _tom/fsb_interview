package fsb.interview.game.controller;

import fsb.interview.game.dto.GameDto;
import fsb.interview.game.service.GameService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/games")
@Slf4j
public class GameController {

  @Autowired private GameService gameService;

  private String getLogId(String string) {
    return "[" + UUID.randomUUID().toString() + "] " + string + ": ";
  }

  @GetMapping("/{name}")
  public ResponseEntity<GameDto> getGame(@PathVariable String name) {
    String logId = this.getLogId("getGame");
    log.info(logId + name);
    GameDto gameDto = this.gameService.getGame(name);
    if (null == gameDto) {
      log.error(logId + "notFound");
      return ResponseEntity.notFound().build();
    } else {
      log.info(logId + "ok " + gameDto);
      return ResponseEntity.ok(gameDto);
    }
  }

  @PostMapping("/")
  public ResponseEntity<GameDto> createGame(@RequestBody GameDto newGameDto) {
    String logId = this.getLogId("createGame");
    log.info(logId + newGameDto);
    GameDto gameDto = this.gameService.createGame(newGameDto);
    if (null == gameDto) {
      log.error(logId + "CONFLICT");
      return ResponseEntity.status(HttpStatus.CONFLICT).build();
    } else {
      log.info(logId + "CREATED " + gameDto);
      return ResponseEntity.status(HttpStatus.CREATED).body(newGameDto);
    }
  }

  @PutMapping("/{name}")
  public ResponseEntity<GameDto> updateGame(
      @PathVariable String name, @RequestBody GameDto updateGameDto) {
    String logId = this.getLogId("updateGame");
    log.info(logId + updateGameDto);
    GameDto gameDto = this.gameService.updateGame(updateGameDto);
    if (null == gameDto) {
      log.error(logId + "notFound");
      return ResponseEntity.notFound().build();
    } else {
      log.info(logId + "ok " + gameDto);
      return ResponseEntity.ok(gameDto);
    }
  }

  @DeleteMapping("/{name}")
  public ResponseEntity<Void> deleteGame(@PathVariable String name) {
    String logId = this.getLogId("deleteGame");
    log.info(logId + name);
    boolean isRemoved = this.gameService.deleteGame(name);
    if (isRemoved) {
      log.info(logId + "noContent");
      return ResponseEntity.noContent().build();
    } else {
      log.error(logId + "notFound");
      return ResponseEntity.notFound().build();
    }
  }
}
