package fsb.interview.game.service;

import fsb.interview.game.dto.GameDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
public class GameServiceTest {

  @Autowired private GameService gameService;

  @Test
  public void testGetGameShouldSuccess() {
    String gameName = UUID.randomUUID().toString();
    this.gameService.createGame(new GameDto(gameName, null, false));
    GameDto gameDtoFromCache = this.gameService.getGame(gameName);
    assertNotNull(gameDtoFromCache);
    assertEquals(gameName, gameDtoFromCache.getName());
    assertEquals(LocalDate.now(), gameDtoFromCache.getDateOfCreation());
    assertEquals(false, gameDtoFromCache.isActive());
  }

  @Test
  public void testGetGameShouldFailureWhenNotExist() {
    GameDto gameDtoFromCache = this.gameService.getGame(UUID.randomUUID().toString());
    assertNull(gameDtoFromCache);
  }

  @Test
  public void testCreateGameShouldSuccess() {
    String gameName = UUID.randomUUID().toString();
    GameDto gameDtoFromCreation = this.gameService.createGame(new GameDto(gameName, null, false));
    assertNotNull(gameDtoFromCreation);
    assertEquals(gameName, gameDtoFromCreation.getName());
    assertEquals(LocalDate.now(), gameDtoFromCreation.getDateOfCreation());
    assertEquals(false, gameDtoFromCreation.isActive());
  }

  @Test
  public void testCreateGameShouldFailureWhenSameName() {
    String gameName = UUID.randomUUID().toString();
    GameDto gameDto = new GameDto(gameName, null, false);
    GameDto gameDtoFromCreation1 = this.gameService.createGame(gameDto);
    GameDto gameDtoFromCreation2 = this.gameService.createGame(gameDto);
    assertNotNull(gameDtoFromCreation1);
    assertNull(gameDtoFromCreation2);
  }

  @Test
  public void testUpdateGameShouldSuccess() {
    String gameName = UUID.randomUUID().toString();
    this.gameService.createGame(new GameDto(gameName, null, false));
    GameDto gameDtoFromUpdating =
        this.gameService.updateGame(new GameDto(gameName, LocalDate.MAX, true));
    assertNotNull(gameDtoFromUpdating);
    assertEquals(gameName, gameDtoFromUpdating.getName());
    assertEquals(LocalDate.MAX, gameDtoFromUpdating.getDateOfCreation());
    assertEquals(true, gameDtoFromUpdating.isActive());
  }

  @Test
  public void testUpdateGameShouldFailureWhenNotExist() {
    GameDto gameDtoFromUpdating =
        this.gameService.updateGame(new GameDto(UUID.randomUUID().toString(), LocalDate.MAX, true));
    assertNull(gameDtoFromUpdating);
  }

  @Test
  public void testDeleteGameShouldReturnTrueWhenExist() {
    String gameName = UUID.randomUUID().toString();
    this.gameService.createGame(new GameDto(gameName, null, false));
    assertEquals(this.gameService.deleteGame(gameName), true);
  }

  @Test
  public void testDeleteGameShouldReturnFalseWhenNotExist() {
    assertEquals(this.gameService.deleteGame(UUID.randomUUID().toString()), false);
  }
}
