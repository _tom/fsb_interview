package fsb.interview.game.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import fsb.interview.game.dto.GameDto;
import fsb.interview.game.service.GameService;
import java.time.LocalDate;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@AutoConfigureMockMvc
@SpringBootTest
public class GameControllerTest {
  @Autowired private MockMvc mockMvc;

  @Autowired private GameService gameService;

  @Test
  public void testGetGameExpectSuccess() throws Exception {
    String gameName = UUID.randomUUID().toString();
    GameDto gameDto = new GameDto(gameName, null, false);
    this.gameService.createGame(gameDto);
    this.mockMvc
        .perform(get("/games/" + gameName))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.name").value(gameName))
        .andExpect(jsonPath("$.dateOfCreation").value(LocalDate.now().toString()))
        .andExpect(jsonPath("$.active").value(false));
  }

  @Test
  public void testGetGameExpectFailure() throws Exception {
    this.mockMvc
        .perform(get("/games/" + UUID.randomUUID().toString()))
        .andExpect(status().isNotFound());
  }

  @Test
  public void testCreateGameExpectSuccess() throws Exception {
    String gameName = UUID.randomUUID().toString();
    this.mockMvc
        .perform(
            post("/games/")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"" + gameName + "\",\"dateOfCreation\":null,\"active\":true}"))
        .andExpect(status().isCreated())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.name").value(gameName))
        .andExpect(jsonPath("$.dateOfCreation").value(LocalDate.now().toString()))
        .andExpect(jsonPath("$.active").value(true));
  }

  @Test
  public void testCreateGameExpectFailure() throws Exception {
    String gameName = UUID.randomUUID().toString();
    GameDto gameDto = new GameDto(gameName, null, false);
    this.gameService.createGame(gameDto);
    this.mockMvc
        .perform(
            post("/games/")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"" + gameName + "\",\"dateOfCreation\":null,\"active\":true}"))
        .andExpect(status().isConflict());
  }

  @Test
  public void testUpdateGameExpectSuccess() throws Exception {
    String gameName = UUID.randomUUID().toString();
    GameDto gameDto = new GameDto(gameName, null, false);
    this.gameService.createGame(gameDto);
    String dateOfCreation = "2000-12-31";
    this.mockMvc
        .perform(
            put("/games/" + gameName)
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                    "{\"name\":\""
                        + gameName
                        + "\",\"dateOfCreation\":\""
                        + dateOfCreation
                        + "\",\"active\":true}"))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.name").value(gameName))
        .andExpect(jsonPath("$.dateOfCreation").value(dateOfCreation))
        .andExpect(jsonPath("$.active").value(true));
  }

  @Test
  public void testUpdateGameExpectFailure() throws Exception {
    String gameName = UUID.randomUUID().toString();
    this.mockMvc
        .perform(
            put("/games/" + gameName)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"" + gameName + "\",\"dateOfCreation\":null,\"active\":true}"))
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDeleteGameExpectSuccess() throws Exception {
    String gameName = UUID.randomUUID().toString();
    GameDto gameDto = new GameDto(gameName, null, false);
    this.gameService.createGame(gameDto);
    this.mockMvc.perform(delete("/games/" + gameName)).andExpect(status().isNoContent());
  }

  @Test
  public void testDeleteGameExpectFailure() throws Exception {
    String gameName = UUID.randomUUID().toString();
    this.mockMvc.perform(delete("/games/" + gameName)).andExpect(status().isNotFound());
  }
}
